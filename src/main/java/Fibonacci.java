import java.time.Duration;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

public class Fibonacci {
    public static final long number = -46; //if n>8471 then stackOverflow is occurred
    //TODO implement cache as another structure because always 2 last values are needed, so size=2 is enough
    static Map<Long, Long> cache;

    private static void initCache() {
        cache = new HashMap<>();
        cache.put(0L, 0L);
        cache.put(1L, 1L);
    }

    private static void printDifference(Instant start, Instant end) {
        System.out.println("fib(" + number + ") time = " + Duration.between(start, end).toMillis());
    }

    public static void main(String[] args) {
        Instant now01 = Instant.now();
        System.out.println("iteration " + fibonacciIteration(number));
        Instant now02 = Instant.now();
        printDifference(now01, now02);

        Instant now03 = Instant.now();
        System.out.println("recursion " + fibonacciRecursion(number));
        Instant now04 = Instant.now();
        printDifference(now03, now04);

        initCache();
        Instant now07 = Instant.now();
        System.out.println("recursCas " + fibonacciRecursionCache(number));
        Instant now09 = Instant.now();
        printDifference(now07, now09);
    }

    public static long fibonacciIteration(long n) {
        if (n == 0) {
            return 0;
        }
        final boolean isPositive = n > 0;
        n = (n < 0) ? -n : n;
        if (n < 3) {
            return isPositive ? 1 : -1;
        }
        long res0 = 1;
        long resn = 1;
        long res;
        for (long i = 3; i <= n; i++) {
            res = res0 + resn;
            res0 = resn;
            resn = res;
        }
        return isPositive ? resn : -resn;
    }

    public static long fibonacciRecursion(long n) {
        if (n == 0) {
            return 0;
        }
        final boolean isPositive = n > 0;
        n = isPositive ? n : -n;
        if (n < 3) {
            return isPositive ? 1 : -1;
        }
        long result = fibonacciRecursion(n - 1) + fibonacciRecursion(n - 2);
        return isPositive ? result : -result;
    }

    public static long fibonacciRecursionCache(long n) {
        final boolean isPositive = n > 0;
        n = isPositive ? n : -n;

        //if (n == 1) { return isPositive ? 1 : -1; }
        // the following if works only for n ==1 so can be replaced with the previous if
        if (cache.containsKey(n)) { return cache.get(n); }

        long result = fibonacciRecursionCache(n - 1) + cache.get(n - 2);
        cache.put(n, result);
        return isPositive ? result : -result;
    }
}